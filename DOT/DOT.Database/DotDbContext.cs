﻿using System.Data.Common;
using System.Data.Entity;
using DOT.Database.Models;

namespace DOT.Database
{
    public class DotDbContext : DbContext
    {
        internal DotDbContext() : base("name=DotConnection")
        {
            System.Data.Entity.Database.SetInitializer(new CreateDatabaseIfNotExists<DotDbContext>());
        }

        public DbSet<DotUser> DotUsers { get; set; }
        public DbSet<DotUserPermission> DotUserPermissions { get; set; }
        public DbSet<DotUserPermissionDotUserRole> DotUserPermissionDotUserRoles { get; set; }
        public DbSet<DotUserRole> DotUserRoles { get; set; }
        public DbSet<DotUserRoleDotUser> DotUserRoleDotUsers { get; set; }
        
        public DotDbContext(DbConnection connection) : base(connection, true)
        {
        }
    }
}
