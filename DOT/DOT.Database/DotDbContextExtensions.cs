﻿using System.Linq;

namespace DOT.Database
{
    public static class DotDbContextExtensions
    {
        /// <summary>
        /// Gets all permissions from all user roles and makes a distinct list.
        /// </summary>
        /// <param name="context"> the db context to operate on</param>
        /// <param name="userId">the user id for which we calculte the list of distinc permissions</param>
        /// <returns>Query to get user permissions</returns>
        public static IQueryable<string> DotUserPermissionsQuery(this DotDbContext context, int userId)
        {
            return (from p in context.DotUserPermissions
                    join rp in context.DotUserPermissionDotUserRoles on p.Id equals rp.DotUserPermissionId
                    join ru in context.DotUserRoleDotUsers on rp.DotUserRoleId equals ru.DotUserRoleId
                    where ru.DotUserId == userId
                    select p.Permission).Distinct();
        }
    }
}
