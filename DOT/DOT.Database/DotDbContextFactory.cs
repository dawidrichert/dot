﻿using System.Data.Common;

namespace DOT.Database
{
    public class DotDbContextFactory
    {
        public static DbConnection DbConnection { get; set; }

        public static DotDbContext Create()
        {
            return new DotDbContext();
        }
    }
}
