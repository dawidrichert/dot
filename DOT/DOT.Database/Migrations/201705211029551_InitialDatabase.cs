namespace DOT.Database.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class InitialDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DotUserPermissionDotUserRoles",
                c => new
                    {
                        DotUserRoleId = c.Int(nullable: false),
                        DotUserPermissionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DotUserRoleId, t.DotUserPermissionId })
                .ForeignKey("dbo.DotUserPermissions", t => t.DotUserPermissionId, cascadeDelete: true)
                .ForeignKey("dbo.DotUserRoles", t => t.DotUserRoleId, cascadeDelete: true)
                .Index(t => t.DotUserRoleId)
                .Index(t => t.DotUserPermissionId);
            
            CreateTable(
                "dbo.DotUserPermissions",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Permission = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DotUserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Role = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DotUserRoleDotUsers",
                c => new
                    {
                        DotUserRoleId = c.Int(nullable: false),
                        DotUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DotUserRoleId, t.DotUserId })
                .ForeignKey("dbo.DotUsers", t => t.DotUserId, cascadeDelete: true)
                .ForeignKey("dbo.DotUserRoles", t => t.DotUserRoleId, cascadeDelete: true)
                .Index(t => t.DotUserRoleId)
                .Index(t => t.DotUserId);
            
            CreateTable(
                "dbo.DotUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 254),
                        PasswordSalt = c.String(maxLength: 300),
                        PasswordHash = c.String(maxLength: 300),
                        PjatkLogin = c.String(),
                        PjatkPassword = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Email, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DotUserRoleDotUsers", "DotUserRoleId", "dbo.DotUserRoles");
            DropForeignKey("dbo.DotUserRoleDotUsers", "DotUserId", "dbo.DotUsers");
            DropForeignKey("dbo.DotUserPermissionDotUserRoles", "DotUserRoleId", "dbo.DotUserRoles");
            DropForeignKey("dbo.DotUserPermissionDotUserRoles", "DotUserPermissionId", "dbo.DotUserPermissions");
            DropIndex("dbo.DotUsers", new[] { "Email" });
            DropIndex("dbo.DotUserRoleDotUsers", new[] { "DotUserId" });
            DropIndex("dbo.DotUserRoleDotUsers", new[] { "DotUserRoleId" });
            DropIndex("dbo.DotUserPermissionDotUserRoles", new[] { "DotUserPermissionId" });
            DropIndex("dbo.DotUserPermissionDotUserRoles", new[] { "DotUserRoleId" });
            DropTable("dbo.DotUsers");
            DropTable("dbo.DotUserRoleDotUsers");
            DropTable("dbo.DotUserRoles");
            DropTable("dbo.DotUserPermissions");
            DropTable("dbo.DotUserPermissionDotUserRoles");
        }
    }
}
