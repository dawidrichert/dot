using Dot.Security.Enums;
using DOT.Database.Models;
using DOT.Domain.Utils;

namespace DOT.Database.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<DotDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DotDbContext context)
        {
            var permAdminSectionDisplay = new DotUserPermission { Id = (int)Permission.AdminSectionDisplay, Permission = Permission.AdminSectionDisplay.GetDescription() };
            AddPermissionIfNotExists(context, ref permAdminSectionDisplay);

            var roleAdministrator = new DotUserRole("Administrator");
            AddRoleIfNotExists(context, ref roleAdministrator);

            context.DotUserPermissionDotUserRoles.RemoveRange(context.DotUserPermissionDotUserRoles.ToList());
            context.DotUserPermissionDotUserRoles.Add(new DotUserPermissionDotUserRole(roleAdministrator, permAdminSectionDisplay));
            
            var adminUser = new DotUser
            {
                Email = "admin@dawidrichert.com",
                PasswordSalt = "uF+GTowcD0u1Z1KQpBf08A==",
                PasswordHash = "UNHil1ryNwFq4Fq5wMio2sFquJw="
            };
            AddUserIfNotExists(context, adminUser, roleAdministrator);
        }

        private static void AddPermissionIfNotExists(DotDbContext context, ref DotUserPermission permission)
        {
            var permissionName = permission.Permission;
            var p = context.DotUserPermissions.FirstOrDefault(x => x.Permission.Equals(permissionName));
            if (p != null)
            {
                permission = p;
                return;
            }
            context.DotUserPermissions.Add(permission);
        }

        private static void AddRoleIfNotExists(DotDbContext context, ref DotUserRole role)
        {
            var roleName = role.Role;
            var r = context.DotUserRoles.FirstOrDefault(x => x.Role.Equals(roleName));
            if (r != null)
            {
                role = r;
                return;
            }
            context.DotUserRoles.Add(role);
        }

        private static void AddUserIfNotExists(DotDbContext context, DotUser user, DotUserRole role)
        {
            if (context.DotUsers.Any(x => x.Email.Equals(user.Email)))
            {
                return;
            }
            context.DotUsers.Add(user);
            context.DotUserRoleDotUsers.Add(new DotUserRoleDotUser
            {
                DotUserRole = role,
                DotUser = user
            });
        }
    }
}
