﻿using System.Data.Entity.Infrastructure;

namespace DOT.Database
{
    public class MigrationsContextFactory : IDbContextFactory<DotDbContext> // This is required by migrations because we don't have public WgDbContext constructor
    {
        public DotDbContext Create()
        {
            return new DotDbContext();
        }
    }
}
