﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Dot.Security;

namespace DOT.Database.Models
{
    public class DotUser
    {
        private static readonly string PjatkPasswordEncryptionPurpose = "PjatkPassword";

        public int Id { get; set; }
        
        [Required]
        [StringLength(254)]
        [Index(IsUnique = true)]
        public string Email { get; set; }
        
        [StringLength(300)]
        public string PasswordSalt { get; set; }

        [StringLength(300)]
        public string PasswordHash { get; set; }

        public string PjatkLogin { get; set; }

        public string PjatkPassword { get; set; }
        
        [NotMapped]
        public string PjatkPasswordDecrypted
        {
            get
            {
                return Encryption.Unprotect(PjatkPassword, PjatkPasswordEncryptionPurpose);
            }
            set
            {
                PjatkPassword = Encryption.Protect(value, PjatkPasswordEncryptionPurpose);
            }
        }

        public bool IsConnectedToPjatk => !string.IsNullOrEmpty(PjatkLogin) && !string.IsNullOrEmpty(PjatkPassword);
        
        public List<DotUserRoleDotUser> DotUserRoleDotUsers { get; set; }
    }
}
