﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOT.Database.Models
{
    public class DotUserPermissionDotUserRole
    {
        public DotUserPermissionDotUserRole()
        {
        }

        public DotUserPermissionDotUserRole(DotUserRole role, DotUserPermission permission)
        {
            DotUserRole = role;
            DotUserPermission = permission;
        }

        [Key, Column(Order = 0)]
        public int DotUserRoleId { get; set; }
        public DotUserRole DotUserRole { get; set; }

        [Key, Column(Order = 1)]
        public int DotUserPermissionId { get; set; }
        public DotUserPermission DotUserPermission { get; set; }
    }
}
