﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DOT.Database.Models
{
    public class DotUserRole
    {
        public DotUserRole()
        {
        }

        public DotUserRole(string role)
        {
            Role = role;
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Role { get; set; }

        public ICollection<DotUserPermissionDotUserRole> DotUserPermissionDotUserRoles { get; set; }
    }
}
