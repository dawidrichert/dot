﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DOT.Database.Models
{
    public class DotUserRoleDotUser
    {
        [Key, Column(Order = 1)]
        public int DotUserId { get; set; }
        public DotUser DotUser { get; set; }

        [Key, Column(Order = 0)]
        public int DotUserRoleId { get; set; }
        public DotUserRole DotUserRole { get; set; }
    }
}
