﻿using System.ComponentModel.DataAnnotations;
using DOT.Resources;

namespace DOT.Domain.Models
{
    public class Credentials
    {
        [Required]
        [Display(Name = "E-mail")]
        [EmailAddress]
        [MaxLength(254)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Global), Name = "Password")]
        [MaxLength(50)]
        public string Password { get; set; }
    }
}
