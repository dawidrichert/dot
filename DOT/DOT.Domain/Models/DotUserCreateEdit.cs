﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DOT.Domain.Models
{
    public class DotUserCreateEdit
    {
        public int Id { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "{0} must be at least {1} characters long"), MaxLength(100, ErrorMessage = "{0} must be equal or less than {1} characters long")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "{0} must be at least {1} characters long"), MaxLength(100, ErrorMessage = "{0} must be equal or less than {1} characters long")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Roles")]
        public List<DotUserRoleSelection> DotUserRolesSelections { get; set; }
    }
}
