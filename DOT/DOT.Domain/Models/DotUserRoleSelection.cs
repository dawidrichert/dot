﻿namespace DOT.Domain.Models
{
    public class DotUserRoleSelection
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsSelected { get; set; }
    }
}