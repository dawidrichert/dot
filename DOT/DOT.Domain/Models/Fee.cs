﻿using System;

namespace DOT.Domain.Models
{
    public class Fee
    {
        public decimal Amount { get; set; }
        public int NumberOfInstallments { get; set; }
        public string Name { get; set; }
        public int InstallmentNumber { get; set; }
        public DateTime DateOfPayment { get; set; }
    }
}
