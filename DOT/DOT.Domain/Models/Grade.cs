﻿using System;

namespace DOT.Domain.Models
{
    public class Grade
    {
        public DateTime Date { get; set; }
        public string ClassesCode { get; set; }
        public string ClassesName { get; set; }
        public int NumberOfHours { get; set; }
        public decimal Value { get; set; }
        public string Leader { get; set; }
        public string Semester { get; set; }
        public string ClassesType { get; set; }

        public string Id => Date.Ticks + ClassesCode;

        public bool IsExam => "exam".Equals(ClassesType.ToLower()) || "egzamin".Equals(ClassesType.ToLower());
    }
}
