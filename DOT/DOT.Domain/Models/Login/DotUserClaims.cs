﻿using System.Collections.Generic;

namespace DOT.Domain.Models.Login
{
    public class DotUserClaims
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public bool IsConnectedToPjatk { get; set; }

        public IList<string> Permissions { get; set; }
    }
}
