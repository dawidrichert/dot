﻿namespace DOT.Domain.Models.Login
{
    public class VerifyDotUserStatus
    {
        public LoginStatus Status { get; set; }
        public int? UserId { get; set; }
        public DotUserClaims DotUserClaims { get; set; }
    }
}
