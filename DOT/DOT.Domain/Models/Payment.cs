﻿using System;

namespace DOT.Domain.Models
{
    public class Payment
    {
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public string Title { get; set; }
        public string Payer { get; set; }
    }
}
