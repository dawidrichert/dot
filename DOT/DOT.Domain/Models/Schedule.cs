﻿using System;

namespace DOT.Domain.Models
{
    public class Schedule
    {
        public string Building { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ClassesCode { get; set; }
        public string ClassesName { get; set; }
        public string Room { get; set; }
        public string ClassesType { get; set; }

        public string Id => StartDate.Ticks + ClassesCode;
        public bool IsLecture => "wykład".Equals(ClassesType.ToLower());
    }
}
