﻿using System.ComponentModel.DataAnnotations;
using DOT.Resources;

namespace DOT.Domain.Models
{
    public class StudentInfo
    {
        private string _incomeAccountNumber;

        [Display(ResourceType = typeof(Global), Name = "FirstName")]
        public string FirstName { get; set; }

        [Display(ResourceType = typeof(Global), Name = "LastName")]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [Display(ResourceType = typeof(Global), Name = "IncomeAccountNumber")]
        public string IncomeAccountNumber
        {
            get { return GetFormattedBankAccountNumber(_incomeAccountNumber); }
            set { _incomeAccountNumber = value; }
        }

        [Display(ResourceType = typeof(Global), Name = "OutcomeAccountNumber")]
        public string OutcomeAccountNumber { get; set; }

        [Display(ResourceType = typeof(Global), Name = "AmountOfReceivables")]
        public decimal AmountOfReceivables { get; set; }

        [Display(ResourceType = typeof(Global), Name = "AmountOfPayments")]
        public decimal AmountOfPayments { get; set; }

        [Display(ResourceType = typeof(Global), Name = "PayoutAmount")]
        public decimal PayoutAmount { get; set; }

        public string Login { get; set; }

        [Display(ResourceType = typeof(Global), Name = "Balance")]
        public decimal Balance { get; set; }

        public string Status { get; set; }

        private static string GetFormattedBankAccountNumber(string text)
        {
            for (var i = text.Length; i > 0; i -= 4)
            {
                text = text.Insert(i, " ");
            }
            return text;
        }
    }
}
