﻿namespace DOT.Domain.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public bool IsConnected { get; set; }
    }
}
