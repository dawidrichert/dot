﻿using System;
using DOT.PJATK.Utils;
using Newtonsoft.Json;

namespace DOT.PJATK.Models
{
    public class Oplaty
    {
        public double Kwota { get; set; }
        public int LiczbaRat { get; set; }
        public string Nazwa { get; set; }
        public int NrRaty { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime TerminPlatnosci { get; set; }
    }
}
