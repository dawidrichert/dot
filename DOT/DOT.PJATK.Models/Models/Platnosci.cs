﻿using System;
using DOT.PJATK.Utils;
using Newtonsoft.Json;

namespace DOT.PJATK.Models
{
    public class Platnosci
    {
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime DataWplaty { get; set; }
        public double Kwota { get; set; }
        public string TytulWplaty { get; set; }
        public string Wplacajacy { get; set; }
    }
}