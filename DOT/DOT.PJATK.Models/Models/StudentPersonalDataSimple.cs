﻿// ReSharper disable InconsistentNaming

namespace DOT.PJATK.Models
{
    public class StudentPersonalDataSimple
    {
        public string Imie { get; set; }
        public string Konto_wplat { get; set; }
        public string Konto_wyplat { get; set; }
        public double Kwota_naleznosci { get; set; }
        public double Kwota_wplat { get; set; }
        public double Kwota_wyplat { get; set; }
        public string Login { get; set; }
        public string Nazwisko { get; set; }
        public double Saldo { get; set; }
        public string Status { get; set; }
    }
}
