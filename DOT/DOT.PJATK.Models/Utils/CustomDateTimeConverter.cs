﻿using Newtonsoft.Json.Converters;

namespace DOT.PJATK.Utils
{
    public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter()
        {
            DateTimeFormat = "dd-MM-yyyy";
        }
    }
}
