﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Reflection;
using System.Threading.Tasks;
using Dot.Security;
using DOT.Database;
using DOT.Database.Models;
using DOT.Domain.Models;
using DOT.Domain.Models.Login;
using log4net;

namespace DOT.Services.Services
{
    public interface IDotUserAuthService
    {
        Task<VerifyDotUserStatus> VerifyDotUser(Credentials credentials);
        Task<DotUserClaims> GetDotUserByEmail(string email);
        Task<DotUserClaims> GetDotUserById(int userId);
    }

    public class DotUserAuthService : IDotUserAuthService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public async Task<VerifyDotUserStatus> VerifyDotUser(Credentials credentials)
        {
            using (var context = DotDbContextFactory.Create())
            {
                // Find the user, and get his salt
                DotUser dotUser = await context.DotUsers.FirstOrDefaultAsync(
                    u => u.Email.Equals(credentials.Email, StringComparison.OrdinalIgnoreCase));

                if (dotUser == null)
                {
                    Log.WarnFormat("User {0} does not exists", credentials.Email);
                    return new VerifyDotUserStatus { Status = LoginStatus.Failure };
                }
                
                // Hash the password for comparison with stored in the db
                var hasher = new PasswordHasher(PasswordHasher.DefaultPasswordHashIterations);
                var passwordHash = hasher.HashPassword(credentials.Password, dotUser.PasswordSalt);

                // Check the email and password hash match
                if (!(dotUser.Email.Equals(credentials.Email, StringComparison.OrdinalIgnoreCase) &&
                      dotUser.PasswordHash.Equals(passwordHash, StringComparison.OrdinalIgnoreCase)))
                {
                    Log.WarnFormat("User {0} invalid credentials", credentials.Email);
                    
                    await context.SaveChangesAsync();
                    
                    return new VerifyDotUserStatus { Status = LoginStatus.Failure };
                }

                // if it went that far then the user is fine and is verified
                Log.DebugFormat("User {0} verified ok", credentials.Email);

                IList<string> permissions = await CalculatePermissions(context, dotUser);
                return new VerifyDotUserStatus
                {
                    UserId = dotUser.Id,
                    Status = LoginStatus.Success,
                    DotUserClaims = ConvertToDotUserClaims(dotUser, permissions),
                };
            }
        }

        public async Task<DotUserClaims> GetDotUserById(int id)
        {
            using (var context = DotDbContextFactory.Create())
            {
                var user = await context.DotUsers
                    .FirstOrDefaultAsync(u => u.Id == id);

                if (user != null)
                {
                    IList<string> permissions = await CalculatePermissions(context, user);
                    return ConvertToDotUserClaims(user, permissions);
                }
            }

            return null;
        }
        
        public async Task<DotUserClaims> GetDotUserByEmail(string email)
        {
            using (var context = DotDbContextFactory.Create())
            {
                var user = await context.DotUsers
                .FirstOrDefaultAsync(u => u.Email.Equals(email, StringComparison.OrdinalIgnoreCase));

                if (user != null)
                {
                    IList<string> permissions = await CalculatePermissions(context, user);
                    return ConvertToDotUserClaims(user, permissions);
                }
            }

            return null;
        }

        /// <summary>
        /// Gets all permissions from all user roles and makes a distinct list
        /// </summary>
        /// <param name="context"> the db context to operate on</param>
        /// <param name="user">the user for which we calculte the list of distinct permissions</param>
        /// <returns>List of user permissions</returns>
        private static async Task<List<string>> CalculatePermissions(DotDbContext context, DotUser user)
        {
            var query = context.DotUserPermissionsQuery(user.Id);

            return await query.ToListAsync();
        }

        private static DotUserClaims ConvertToDotUserClaims(DotUser user, IList<string> permissions)
        {
            return new DotUserClaims
            {
                Id = user.Id,
                Email = user.Email,
                IsConnectedToPjatk = user.IsConnectedToPjatk,
                Permissions = permissions
            };
        }
    }
}
