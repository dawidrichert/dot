﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Dot.Security;
using DOT.Database;
using DOT.Database.Models;
using DOT.Domain.Models;
using DOT.Services.Utils;
using log4net;

namespace DOT.Services.Services
{
    public class DotUserService
    {
        private readonly PjatkService _pjatkService;

        public DotUserService()
        {
            _pjatkService = new PjatkService();
        }

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public async Task<bool> ConnectAccount(string login, string password)
        {
            var validCredentials = await _pjatkService.VerifyCredentials(login, password);
            if (validCredentials)
            {
                using (var db = DotDbContextFactory.Create())
                {
                    var user = await db.DotUsers.FirstOrDefaultAsync(x => x.Id == UserUtilities.CurrentUserId);
                    if (user != null)
                    {
                        user.PjatkLogin = login;
                        user.PjatkPasswordDecrypted = password;
                        await db.SaveChangesAsync();
                    }
                }
            }
            return validCredentials;
        }

        public async Task<bool> DisconnectAccount()
        {
            using (var db = DotDbContextFactory.Create())
            {
                var user = await db.DotUsers.FirstOrDefaultAsync(x => x.Id == UserUtilities.CurrentUserId);
                if (user != null)
                {
                    user.PjatkLogin = null;
                    user.PjatkPassword = null;
                    await db.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }

        public async Task<int> CreateDotUser(DotUserCreateEdit model)
        {
            Log.Info("Creating new User");
            var dotUser = new DotUser();
            FillDotUserDatabaseObject(model, dotUser);
            using (var db = DotDbContextFactory.Create())
            {
                db.DotUsers.Add(dotUser);
                await db.SaveChangesAsync();
                Log.InfoFormat("Added new User with the above roles to database with id: {0}", dotUser.Id);
                    
                return dotUser.Id;
            }
        }

        public async Task RemoveUser(int id)
        {
            Log.InfoFormat("Removing User with id: {0}", id);
            using (var db = DotDbContextFactory.Create())
            {
                var user = await db.DotUsers.FirstOrDefaultAsync(x => x.Id == id);
                if (user != null)
                {
                    db.DotUsers.Remove(user);
                    await db.SaveChangesAsync();
                    Log.InfoFormat("Removed User with id: {0}", id);
                }
            }
        }

        public ICollection<User> GetUsers()
        {
            using (var db = DotDbContextFactory.Create())
            {
                return db.DotUsers
                    .ToList()
                    .Select(x => new User
                    {
                        Id = x.Id,
                        Email = x.Email,
                        IsConnected = x.IsConnectedToPjatk
                    }).ToList();
            }
        }
        
        private static void FillDotUserDatabaseObject(DotUserCreateEdit domainModel, DotUser databaseModel)
        {
            var hasher = new PasswordHasher(PasswordHasher.DefaultPasswordHashIterations);
            var passwordSalt = hasher.GenerateSalt();
            var passwordHash = hasher.HashPassword(domainModel.Password, passwordSalt);
            
            databaseModel.Email = domainModel.Email;
            databaseModel.PasswordSalt = passwordSalt;
            databaseModel.PasswordHash = passwordHash;
        }
    }
}
