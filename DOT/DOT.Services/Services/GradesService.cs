﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DOT.Domain.Models;
using DOT.Services.Utils;

namespace DOT.Services.Services
{
    public class GradesService
    {
        private readonly PjatkService _pjatkService;

        public GradesService()
        {
            _pjatkService = new PjatkService();
        }

        public async Task<ICollection<Grade>> GetGrades()
        {
            var data = await _pjatkService.GetStudentGrades();
            return data.Select(ocena => new Grade
            {
                Date = ocena.Data,
                ClassesName = LocalizationUtils.LocalizedText(ocena.NazwaPrzedmiotu, ocena.NazwaPrzedmiotuAng),
                ClassesCode = ocena.KodPrzedmiotu,
                NumberOfHours = ocena.LiczbaGodzin,
                Value = ocena.Ocena.ToDecimal(),
                Leader = ocena.Prowadzacy,
                Semester = GetTranslatedSemesterName(ocena.Semestr),
                ClassesType = LocalizationUtils.LocalizedText(ocena.Zaliczenie, ocena.ZaliczenieAng)
            })
            .OrderByDescending(x => x.Date)
            .ToList();
        }

        private static string GetTranslatedSemesterName(string semester)
        {
            return LocalizationUtils.IsUserLanguagePolish()
                ? semester
                : semester
                    .Replace("zimowy", "winter")
                    .Replace("letni", "summer");
        }
    }
}
