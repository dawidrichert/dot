﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DOT.Domain.Models;

namespace DOT.Services.Services
{
    public class PaymentsService
    {
        private readonly PjatkService _pjatkService;

        public PaymentsService()
        {
            _pjatkService = new PjatkService();
        }

        public async Task<ICollection<Payment>> GetPayments()
        {
            var data = await _pjatkService.GetStudentPayments();
            return data.Select(p => new Payment
                {
                    Date = p.DataWplaty,
                    Amount = Convert.ToDecimal(p.Kwota),
                    Title = p.TytulWplaty,
                    Payer = p.Wplacajacy
                })
                .OrderByDescending(f => f.Date)
                .ToList();
        }

        public async Task<ICollection<Fee>> GetFees()
        {
            var data = await _pjatkService.GetStudentFees();
            return data.Select(f => new Fee
                {
                    Amount = Convert.ToDecimal(f.Kwota),
                    NumberOfInstallments = f.LiczbaRat,
                    Name = f.Nazwa,
                    InstallmentNumber = f.NrRaty,
                    DateOfPayment = f.TerminPlatnosci
                })
                .OrderByDescending(f => f.DateOfPayment)
                .ToList();
        }
    }
}
