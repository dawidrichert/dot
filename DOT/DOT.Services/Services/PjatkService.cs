﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using DOT.Database;
using DOT.PJATK.Models;
using DOT.Services.Utils;
using Newtonsoft.Json;

namespace DOT.Services.Services
{
    public class PjatkService
    {
        private const string BaseUrl = "https://ws.pjwstk.edu.pl/test/Service.svc/XmlService";
        private const string EmailSuffix = "@pjwstk.edu.pl";
        
        public async Task<StudentPersonalData> GetStudentPersonalData(NetworkCredential credentials = null)
        {
            return await Get<StudentPersonalData>("GetStudentPersonalDataJson", credentials);
        }

        public async Task<ICollection<Oceny>> GetStudentGrades(NetworkCredential credentials = null)
        {
            return await Get<ICollection<Oceny>>("GetStudentGradesJson", credentials);
        }

        public async Task<string> GetPersonImage(NetworkCredential credentials = null)
        {
            return await Get<string>("GetPersonImageJson", credentials);
        }

        public async Task<ICollection<StudentSchedule>> GetStudentSchedule(DateTime from, DateTime to, NetworkCredential credentials = null)
        {
            return await Get<ICollection<StudentSchedule>>($"GetStudentScheduleJson?begin={from:yyyy-MM-dd}&end={to:yyyy-MM-dd}", credentials);
        }

        public async Task<ICollection<Platnosci>> GetStudentPayments(NetworkCredential credentials = null)
        {
            return await Get<ICollection<Platnosci>>("GetStudentPaymentsJson", credentials);
        }

        public async Task<ICollection<Oplaty>> GetStudentFees(NetworkCredential credentials = null)
        {
            return await Get<ICollection<Oplaty>>("GetStudentFeesJson", credentials);
        }

        public async Task<StudentPersonalDataSimple> GetStudentPersonalDataSimple(NetworkCredential credentials = null)
        {
            return await Get<StudentPersonalDataSimple>("GetStudentPersonalDataSimpleJson", credentials);
        }

        private static async Task<T> Get<T>(string url, NetworkCredential credentials = null) where T : class
        {
            if (credentials == null)
            {
                using (var db = DotDbContextFactory.Create())
                {
                    var user = await db.DotUsers.FirstOrDefaultAsync(x => x.Id == UserUtilities.CurrentUserId);
                    if (user != null)
                    {
                        credentials = GetNetworkCredential(user.PjatkLogin, user.PjatkPasswordDecrypted);
                    }
                }
            }

            try
            {
                string noCache;
                if (url.Contains("?"))
                {
                    noCache = "&nocache=" + DateTime.Now.Ticks;
                }
                else
                {
                    noCache = "?nocache=" + DateTime.Now.Ticks;
                }

                var fullUrl = $"{BaseUrl}/{url}{noCache}";

                var request = (HttpWebRequest)WebRequest.Create(fullUrl);
                request.Method = "GET";
                request.Credentials = credentials;

                using (var response = (HttpWebResponse)await request.GetResponseAsync())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        if (stream == null)
                        {
                            return null;
                        }
                        using (var reader = new StreamReader(stream))
                        {
                            var result = await reader.ReadToEndAsync();
                            return JsonConvert.DeserializeObject<T>(result);
                        }
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        public async Task<bool> VerifyCredentials(string login, string password)
        {
            var data = await GetStudentPersonalDataSimple(GetNetworkCredential(login, password));
            return data != null;
        }

        private static NetworkCredential GetNetworkCredential(string login, string password)
        {
            return login.Contains(EmailSuffix)
                ? new NetworkCredential(login, password)
                : new NetworkCredential($"{login}{EmailSuffix}", password);
        }
    }
}
