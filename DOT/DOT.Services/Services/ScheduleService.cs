﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DOT.Domain.Models;

namespace DOT.Services.Services
{
    public class ScheduleService
    {
        private readonly PjatkService _pjatkService;

        public ScheduleService()
        {
            _pjatkService = new PjatkService();
        }

        public async Task<ICollection<Schedule>> GetSchedules(DateTime from, DateTime to)
        {
            var data = await _pjatkService.GetStudentSchedule(from, to);
            return data.Select(s => new Schedule
            {
                Building = s.Budynek,
                StartDate = s.Data_roz,
                EndDate = s.Data_zak,
                ClassesCode = s.Kod,
                ClassesName = s.Nazwa,
                Room = s.Nazwa_sali,
                ClassesType = s.TypZajec
            })
            .OrderBy(x => x.StartDate)
            .ToList();
        }
    }
}
