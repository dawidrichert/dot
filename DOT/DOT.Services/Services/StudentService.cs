﻿using System;
using System.Threading.Tasks;
using DOT.Domain.Models;

namespace DOT.Services.Services
{
    public class StudentService
    {
        private readonly PjatkService _pjatkService;

        public StudentService()
        {
            _pjatkService = new PjatkService();
        }

        public async Task<StudentInfo> GetStudentInfo()
        {
            var data = await _pjatkService.GetStudentPersonalDataSimple();
            return new StudentInfo
            {
                FirstName = data.Imie,
                LastName = data.Nazwisko,
                IncomeAccountNumber = data.Konto_wplat,
                OutcomeAccountNumber = data.Konto_wyplat,
                AmountOfReceivables = Convert.ToDecimal(data.Kwota_naleznosci),
                AmountOfPayments = Convert.ToDecimal(data.Kwota_wplat),
                PayoutAmount = Convert.ToDecimal(data.Kwota_wyplat),
                Login = data.Login,
                Balance = Convert.ToDecimal(data.Saldo),
                Status = data.Status
            };
        }
    }
}
