﻿using System;
using System.Linq.Expressions;
using System.Threading;

namespace DOT.Services.Utils
{
    public static class LocalizationUtils
    {
        private const string PlPl= "pl-PL";
        private const string EnGb = "en-GB";

        public static Expression<Func<TSource, string>> LocalizedProperty<TSource>(Expression<Func<TSource, string>> funcPl, Expression<Func<TSource, string>> func)
        {
            return UserLanguage() == PlPl ? funcPl : func;
        }

        public static string LocalizedText(string valuePl, string value)
        {
            return IsUserLanguagePolish() ? valuePl ?? value : value;
        }
        
        public static bool IsUserLanguageEnglish()
        {
            return UserLanguage() == EnGb;
        }

        public static bool IsUserLanguagePolish()
        {
            return UserLanguage() == PlPl;
        }

        private static string UserLanguage()
        {
            return Thread.CurrentThread.CurrentCulture.Name;
        }
    }
}