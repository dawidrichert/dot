﻿using System.Globalization;

namespace DOT.Services.Utils
{
    public static class NumberExtensions
    {
        public static decimal ToDecimal(this string value)
        {
            return decimal.Parse(value.Replace(",", "."), CultureInfo.InvariantCulture);
        }
    }
}
