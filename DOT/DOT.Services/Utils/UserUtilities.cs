﻿using System.Linq;

namespace DOT.Services.Utils
{
    public class UserUtilities
    {
        public static int CurrentUserId
        {
            get
            {
                var user = System.Threading.Thread.CurrentPrincipal;
                var currentIdentity = (System.Security.Claims.ClaimsIdentity)user.Identity;
                var userId = currentIdentity.Claims.Single(p => p.Type.EndsWith("nameidentifier")).Value;
                return int.Parse(userId);
            }
        }
    }
}
