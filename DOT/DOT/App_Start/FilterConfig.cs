﻿using System.Web.Mvc;
using DOT.Errors;
using DOT.Filters;
using DOT.Security;

namespace DOT
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ErrorHandler.AiHandleErrorAttribute());
            filters.Add(new LocalizationAttribute());
            filters.Add(new AuthorizePermissionAttribute());
            filters.Add(new HandleDotErrorAttribute());
        }
    }
}
