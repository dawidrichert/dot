﻿using System;
using System.Web.Helpers;
using DOT.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace DOT
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity =
                        SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, DotUserSecurityInfo, string>(
                            // This assures the database is called per each request for user details
                            // If you want to cache all claims in the cookie then change to something bigger
                            // But we do not want to store those values in the cookie for DOT so it's set to 0 seconds.
                            validateInterval: TimeSpan.FromSeconds(0),
                            regenerateIdentityCallback: (manager, user) => user.GenerateUserIdentityAsync(manager),
                            getUserIdCallback: (claimsIdentity) => claimsIdentity.GetUserId<string>()),
                },
                SlidingExpiration = true,
                CookieName = "DOT.AppCookie"
            });
            
            // AntiForgery Tokens for CSRF protection
            AntiForgeryConfig.CookieName = "DOT.AFR";
        }
    }
}