﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DOT.Attributes;

namespace DOT
{
    public class ValidatorConfig
    {
        public static void RegisterValidators()
        {
            ClientDataTypeModelValidatorProvider.ResourceClassKey = "ValidationMessages";
            DefaultModelBinder.ResourceClassKey = "ValidationMessages";

            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredAttribute), typeof(LocalizableRequiredAttributeAdapter));
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(EmailAddressAttribute), typeof(LocalizableEmailAddressAttributeAdapter));
        }
    }
}