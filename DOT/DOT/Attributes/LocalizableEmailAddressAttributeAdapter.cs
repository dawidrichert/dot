﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DOT.Utils;
using Resources;

namespace DOT.Attributes
{
    public class LocalizableEmailAddressAttributeAdapter : DataAnnotationsModelValidator<EmailAddressAttribute>
    {
        public LocalizableEmailAddressAttributeAdapter(ModelMetadata metadata, ControllerContext context, EmailAddressAttribute attribute) : base(metadata, context, attribute)
        {
            attribute.ErrorMessage = ValidationMessages.ResourceManager.GetString("EmailAddressAttributeInvalid", Localizations.GetUserCultureInfo(context));
        }
    }
}