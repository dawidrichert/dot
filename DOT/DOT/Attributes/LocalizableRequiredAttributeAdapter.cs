﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace DOT.Attributes
{
    public class LocalizableRequiredAttributeAdapter : RequiredAttributeAdapter
    {
        public LocalizableRequiredAttributeAdapter(ModelMetadata metadata, ControllerContext context, RequiredAttribute attribute) : base(metadata, context, attribute)
        {
            attribute.ErrorMessageResourceType = typeof(ValidationMessages);
            attribute.ErrorMessageResourceName = "PropertyValueRequired";
        }
    }
}