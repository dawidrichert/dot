﻿using System.Threading.Tasks;
using System.Web.Mvc;
using DOT.Security;
using DOT.Services.Services;

namespace DOT.Controllers
{
    [AuthorizePermission(Permissions = "AdminSectionDisplay")]
    public class AdminController : Controller
    {
        private readonly DotUserService _dotUserService;

        public AdminController()
        {
            _dotUserService = new DotUserService();
        }

        public ActionResult Users()
        {
            var users = _dotUserService.GetUsers();

            return View(users);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveUser(int id)
        {
            var dotUserService = new DotUserService();
            await dotUserService.RemoveUser(id);

            return RedirectToAction("Users", "Admin");
        }
    }
}