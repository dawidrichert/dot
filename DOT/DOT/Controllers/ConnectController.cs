﻿using System.Threading.Tasks;
using System.Web.Mvc;
using DOT.Resources;
using DOT.Security;
using DOT.Services.Services;
using DOT.Utils;
using DOT.ViewModels;

namespace DOT.Controllers
{
    public class ConnectController : Controller
    {
        private readonly DotUserService _dotUserService;

        public ConnectController()
        {
            _dotUserService = new DotUserService();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(ConnectAccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _dotUserService.ConnectAccount(model.Login, model.Password);
                if (result)
                {
                    User.AddUpdateClaim(ClaimsPrincipalExtensions.DotIsConnectedToPjatkClaimName, true.ToString());
                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("", Global.InvalidCredentials);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disconnect()
        {
            if (ModelState.IsValid)
            {
                var result = await _dotUserService.DisconnectAccount();
                if (result)
                {
                    User.AddUpdateClaim(ClaimsPrincipalExtensions.DotIsConnectedToPjatkClaimName, false.ToString());
                }
            }
            return RedirectToAction("Index", "Home");
        }
    }
}