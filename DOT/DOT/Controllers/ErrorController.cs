﻿using System.Web.Mvc;

namespace DOT.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            return View("Error/Error");
        }

        public ActionResult Http403()
        {
            return View("Error/Http403");
        }

        public ActionResult Http404()
        {
            return View("Error/Http404");
        }

        public ActionResult Http500()
        {
            return View("Error/Http500");
        }
    }
}