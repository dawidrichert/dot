﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using DOT.Security;
using DOT.Services.Services;
using DOT.ViewModels;

namespace DOT.Controllers
{
    [AuthorizeIsConnectedToPjatk]
    public class GradesController : Controller
    {
        private readonly GradesService _gradesService;

        public GradesController()
        {
            _gradesService = new GradesService();
        }

        public async Task<ActionResult> Index()
        {
            var data = await _gradesService.GetGrades();

            var grades = data
                .GroupBy(x => x.Semester)
                .Select(group => new GradesViewModel
                {
                    Name = group.Key,
                    Grades = group.ToList()
                }).ToList();
            
            return View(grades);
        }
    }
}