﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DOT.Security;
using DOT.Services.Services;

namespace DOT.Controllers
{
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            if (Request.IsAuthenticated)
            {
                if (HttpContext.GetOwinContext().Authentication.User.IsConnectedToPjatk())
                {
                    var pjatkService = new PjatkService();
                    var studentService = new StudentService();
                    ViewBag.ImageBase64 = await pjatkService.GetPersonImage();
                    ViewBag.StudentInfo = await studentService.GetStudentInfo();
                }
                return View("Dashboard");
            }
            return View();
        }
    }
}