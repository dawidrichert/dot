﻿using System.Web;
using System.Web.Mvc;
using DOT.Utils;

namespace DOT.Controllers
{
    [AllowAnonymous]
    public class LanguageController : Controller
    {
        public ActionResult English()
        {
            Response.Cookies.Add(new HttpCookie(Localizations.LanguageCookieName, Localizations.EnglishCultureCode));

            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.AbsoluteUri);
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Polish()
        {
            Response.Cookies.Add(new HttpCookie(Localizations.LanguageCookieName, Localizations.PolishCultureCode));

            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.AbsoluteUri);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}