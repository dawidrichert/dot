﻿using System.Threading.Tasks;
using System.Web.Mvc;
using DOT.Security;
using DOT.Services.Services;
using DOT.ViewModels;

namespace DOT.Controllers
{
    [AuthorizeIsConnectedToPjatk]
    public class PaymentsController : Controller
    {
        private readonly PaymentsService _paymentsService;
        private readonly StudentService _studentService;

        public PaymentsController()
        {
            _paymentsService = new PaymentsService();
            _studentService = new StudentService();
        }

        public async Task<ActionResult> Index()
        {
            return View(new PaymentsViewModel
            {
                Payments = await _paymentsService.GetPayments(),
                Fees = await _paymentsService.GetFees(),
                StudentInfo = await _studentService.GetStudentInfo()
            });
        }
    }
}