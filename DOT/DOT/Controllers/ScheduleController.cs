﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using DOT.Security;
using DOT.Services.Services;
using DOT.ViewModels;

namespace DOT.Controllers
{
    [AuthorizeIsConnectedToPjatk]
    public class ScheduleController : Controller
    {
        private readonly ScheduleService _scheduleService;

        public ScheduleController()
        {
            _scheduleService = new ScheduleService();
        }

        public async Task<ActionResult> Index()
        {
            var today = DateTime.Today;
            var data = await _scheduleService.GetSchedules(today, today.AddYears(1));

            var schedules = data
                .GroupBy(x => x.StartDate.Date)
                .Select(group => new ScheduleViewModel
                {
                    Date = group.Key,
                    Schedules = group.ToList()
                }).ToList();

            return View(schedules);
        }
    }
}