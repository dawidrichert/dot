﻿using System.Net;
using System.Web.Hosting;

namespace DOT.Errors
{
    public static class ErrorLinks
    {
        public static string GetErrorLink(int status)
        {
            switch (status)
            {
                case 403:
                    return HostingEnvironment.MapPath("~/Content/Errors/Error403.html");

                case 404:
                    return HostingEnvironment.MapPath("~/Content/Errors/Error404.html");

                case 500:
                    return HostingEnvironment.MapPath("~/Content/Errors/Error500.html");

                default:
                    return HostingEnvironment.MapPath("~/Content/Errors/Error.html");
            }
        }

        public static int MapToWebBrowserHttpStatus(int status)
        {
            switch (status)
            {
                case 403:
                    return (int)HttpStatusCode.Forbidden;

                case 404:
                    return (int)HttpStatusCode.OK; // this is for nginx to show the correct error page

                case 500:
                    return (int)HttpStatusCode.OK; // this is for nginx to show the correct error page

                default:
                    return (int)HttpStatusCode.OK; // this is for nginx to show the correct error page
            }
        }

        public static string GetErrorViewLink(int status)
        {
            switch (status)
            {
                case 403:
                    return "Error/Http403";

                case 404:
                    return "Error/Http404";

                case 500:
                    return "Error/Http500";

                default:
                    return "Error/";
            }
        }
    }
}
