﻿using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using DOT.Security;
using log4net;

namespace DOT.Errors
{
    public class HandleDotErrorAttribute : HandleErrorAttribute
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public new string View { get; set; }

        public override void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException(nameof(filterContext));
            }
            if (filterContext.IsChildAction)
            {
                return;
            }

            // If custom errors are disabled, we need to let the normal ASP.NET exception handler
            // execute so that the user can see useful debugging information.
            if (filterContext.ExceptionHandled || !filterContext.HttpContext.IsCustomErrorEnabled)
            {
                return;
            }

            Exception exception = filterContext.Exception;
            var status = new HttpException(null, exception).GetHttpCode();

            if (!ExceptionType.IsInstanceOfType(exception))
            {
                return;
            }

            if (exception is PermissionException)
            {
                status = (int)HttpStatusCode.Forbidden;
            }
            else
            {
                // {"Response status code does not indicate success: 404 (Not Found)."}
                var httpEx = exception as HttpRequestException;
                if (httpEx?.Message != null && httpEx.Message.Contains("404 (Not Found)"))
                {
                    status = (int)HttpStatusCode.NotFound;
                }
            }

            // Get details
            var controllerName = (string)filterContext.RouteData.Values["controller"];
            var actionName = (string)filterContext.RouteData.Values["action"];
            var model = new HandleErrorInfo(filterContext.Exception, controllerName, actionName);

            // Log the error
            Log.Error($"Handled Error in controller {controllerName}, action {actionName}, status {status}, status for web browser {ErrorLinks.MapToWebBrowserHttpStatus(status)} - {exception.Message}",
                exception);

            // Return the error page, or a view if someone provided for a particualr method and does not want to use default
            filterContext.Result = new ViewResult
            {
                ViewName = string.IsNullOrEmpty(View) ? ErrorLinks.GetErrorViewLink(status) : View,
                MasterName = Master,
                ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
                TempData = filterContext.Controller.TempData
            };

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = ErrorLinks.MapToWebBrowserHttpStatus(status);

            // Certain versions of IIS will sometimes use their own error page when
            // they detect a server error. Setting this property indicates that we
            // want it to try to render ASP.NET MVC's error page instead.
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }
    }
}