﻿using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using DOT.Utils;

namespace DOT.Filters
{
    public class LocalizationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var cookie = filterContext.HttpContext.Request.Cookies[Localizations.LanguageCookieName];
            if (cookie != null)
            {
                var culture = CultureInfo.CreateSpecificCulture(cookie.Value);
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}