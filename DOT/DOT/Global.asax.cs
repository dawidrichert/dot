﻿using System;
using System.Data.Entity.Migrations;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DOT.Database.Migrations;
using DOT.Errors;
using log4net;

namespace DOT
{
    public class MvcApplication : HttpApplication
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected void Application_Start()
        {
            var configuration = new Configuration();
            var migrator = new DbMigrator(configuration);
            migrator.Update();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ValidatorConfig.RegisterValidators();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Get error details
            var exception = Server.GetLastError();
            var lastError = exception as HttpException;
            var status = lastError?.GetHttpCode() ?? 500;
            var url = Request.Url.OriginalString;
            var msg = exception?.Message ?? "";

            // Log error details
            Log.Error($"Application Error: {status} - {msg} , url: {url}", exception);

            // Clear current context
            Response.Clear();
            Server.ClearError();

            // Prepare response with the error page
            try
            {
                Response.TrySkipIisCustomErrors = true;
                Response.Redirect(string.Concat("~/", ErrorLinks.GetErrorViewLink(status)));
            }
            catch (Exception)
            {
                // Ignore it as in some cases it's too late to modify the response.
            }
        }
    }
}
