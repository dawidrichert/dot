﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace DOT.HtmlHelpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString ViewFormLineCurrencyDisplay<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression)
        {
            return ViewFormLineDisplay(html, expression, true);
        }

        public static MvcHtmlString ViewFormLineDisplay<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, bool showCurrency = false)
        {
            return new MvcHtmlString($@"<div class=""row"">
                                            <div class=""col-md-offset-4 col-md-2 text-right"">
                                                <strong>{html.DisplayNameFor(expression)}</strong>
                                            </div>
                                            <div class=""col-md-3"">
                                                <span>{html.DisplayFor(expression)}" + (showCurrency ? " zł" : "") + @"</span>
                                            </div>
                                        </div>");
        }

        public static MvcPanelItem BeginPanelItem<TModel>(this HtmlHelper<TModel> html, string label)
        {
            return new MvcPanelItem(html, label);
        }
    }
}