﻿using System;
using System.IO;
using System.Web.Mvc;

namespace DOT.HtmlHelpers
{
    public abstract class MvcDisposableItem : IDisposable
    {
        private readonly FormContext _originalFormContext;
        private readonly ViewContext _viewContext;
        private readonly TextWriter _writer;
        private bool _disposed;

        protected abstract void End();

        protected MvcDisposableItem(HtmlHelper html)
        {
            if (html.ViewContext == null)
            {
                throw new ArgumentNullException(nameof(html));
            }

            _viewContext = html.ViewContext;
            _originalFormContext = html.ViewContext.FormContext;
            _writer = html.ViewContext.Writer;
            html.ViewContext.FormContext = new FormContext();
        }

        public void AddContent(string content)
        {
            _writer.Write(content);
        }

        public void EndForm()
        {
            Dispose(true);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                _disposed = true;
                End();

                if (_viewContext != null)
                {
                    _viewContext.OutputClientValidation();
                    _viewContext.FormContext = _originalFormContext;
                }
            }
        }
    }
}
