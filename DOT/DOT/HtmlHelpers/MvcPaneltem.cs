﻿using System.Web.Mvc;

namespace DOT.HtmlHelpers
{
    public class MvcPanelItem : MvcDisposableItem
    {
        private const string PanelBegin = @"
                <div class='panel panel-primary'>
                    <div class='panel-heading'>
                        <h3 class='panel-title'>{0}</h3>
                    </div>
                    <div class='panel-body'>";

        private const string PanelEnd = @"
                    </div>
                </div>";

        public MvcPanelItem(HtmlHelper html, string label) : base(html)
        {
            AddContent(string.Format(PanelBegin, label));
        }

        protected override void End()
        {
            AddContent(PanelEnd);
        }
    }
}