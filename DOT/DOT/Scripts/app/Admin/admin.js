﻿var DOT = (function ($, pub) {
    pub.admin = {

        initUsersForm: function () {
            var confirmed = false;
            $(".removeUserForm").submit(function (e) {
                if (!confirmed) {
                    e.preventDefault();
                    var form = $(this);
                    var eventData = {};

                    eventData.data = {
                        title: DOT.JSResources.removeUserTitle,
                        content: DOT.JSResources.removeUserMessage,
                        yesButtonCallback: function () {
                            confirmed = true;
                            form.submit();
                        }
                    }

                    showSimpleModal_YES_NO(eventData);
                }
                confirmed = false;
            });
        }
    };

    return pub;
}(jQuery, DOT || {}));