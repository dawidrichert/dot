﻿var DOT = (function ($, pub) {
    pub.dashboard = {

        initViewForm: function () {
            var confirmed = false;
            $("#DisconnectForm").submit(function (e) {
                if (!confirmed) {
                    e.preventDefault();
                    var eventData = {};

                    eventData.data = {
                        title: DOT.JSResources.disconnect,
                        content: DOT.JSResources.removeConnectionMessage,
                        yesButtonCallback: function () {
                            confirmed = true;
                            $("#DisconnectForm").submit();
                        }
                    }

                    showSimpleModal_YES_NO(eventData);
                }
                confirmed = false;
            });
        }
    };

    return pub;
}(jQuery, DOT || {}));