﻿
/**
* This method could be used to simply show modal dialog with YES/NO buttons.
* It is not required to pass all arguments but you should provide first two - title, content.
*
* Callback function shouldn't support any arguments but it is implemented like this to support
* specific requirements for reset password.
* eventData = {
*     title: "", required
*     content: "", required
*     yesButtonCallback: function
*     noButtonCallback: function
*     onShow: function to be run on modal show (i.e. to run some javascript handlers inside the window)
*     focusOn: jquery selector (i.e. #filterName) of the input that should be focused when window is opened
*     yesText: text to be shown as yes ("Yes" is default if not provided)
*     noText: text to be shown as no ("No" is default if not provided)
*     primaryButton: yes, no - defines which of the buttons will be highlighted as the default button, default is Yes button
*     ...
* }
*
*/
function showSimpleModal_YES_NO(eventData) {
    var yesText = DOT.JSResources.yes;
    var noText = DOT.JSResources.no;

    if (eventData.data.yesText) {
        yesText = eventData.data.yesText;
    }

    if (eventData.data.noText) {
        noText = eventData.data.noText;
    }

    var primaryButtonHtml = '<button type="button" id="modal-yes-btn" class="btn btn-danger modal-confirm-button">' + yesText + '</button> \
                                <button type="button" id="modal-no-btn" class="btn btn-default" data-dismiss="modal">' + noText + '</button>';

    if (eventData.data.primaryButton === "no") {
        primaryButtonHtml = '<button type="button" id="modal-yes-btn" class="btn btn-default modal-confirm-button">' + yesText + '</button> \
                                <button type= "button" id= "modal-no-btn" class="btn btn-danger" data- dismiss="modal"> ' + noText + '</button>';
    }

    var mod = $('<div class="modal fade dialog-prompt" data-backdrop="static"> \
         <div class="modal-dialog"> \
             <div class="modal-content"> \
                 <div class="modal-header"> \
                     <h4 class="modal-title">' + eventData.data.title + '</h4> \
                 </div> \
                 <div class="modal-body"> \
                     <p>' + eventData.data.content + '</p> \
                 </div> \
                 <div class="modal-footer buttons"> '
        + primaryButtonHtml
        + '</div> \
             </div> \
         </div> \
     </div>');

    mod.one('show.bs.modal', function () {
        if (eventData.data.onShow) {
            eventData.data.onShow($(this));
        }
    });

    mod.one("click", "button.modal-confirm-button", function () {
        //invoke callback if provided and pass eventData object
        if (eventData && eventData.data && eventData.data.yesButtonCallback && typeof (eventData.data.yesButtonCallback) == "function") {
            eventData.data.yesButtonCallback(eventData);
        }

        //close modal
        $('.modal.fade.dialog-prompt').modal("hide");
    });

    mod.one("click", "#modal-no-btn", function () {
        //invoke callback if provided and pass eventData object
        if (eventData && eventData.data && eventData.data.noButtonCallback && typeof (eventData.data.noButtonCallback) == "function") {
            eventData.data.noButtonCallback(eventData);
        }

        //close modal
        $('.modal.fade.dialog-prompt').modal("hide");
    });

    mod.one('shown.bs.modal', function () {
        if (eventData.data.focusOn) {
            $(this).find(eventData.data.focusOn).focus();
        }
    });

    mod.one('hidden.bs.modal', function () {
        $(this).remove();
    });

    mod.modal();
}