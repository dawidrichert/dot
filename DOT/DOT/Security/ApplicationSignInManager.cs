﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using DOT.Domain.Models;
using DOT.Domain.Models.Login;
using DOT.Services.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace DOT.Security
{
    public class ApplicationSignInManager : SignInManager<DotUserSecurityInfo, string>
    {
        public IDotUserAuthService AuthService { get; set; }

        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager, IDotUserAuthService authService)
            : base(userManager, authenticationManager)
        {
            AuthService = authService;
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication, new DotUserAuthService());
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(DotUserSecurityInfo user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public override Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            throw new InvalidOperationException("Please use Task<LoginStatus> PasswordSignInAsync method instead.");
        }

        public override Task<SignInStatus> TwoFactorSignInAsync(string provider, string code, bool isPersistent, bool rememberBrowser)
        {
            throw new InvalidOperationException("Please use Task<LoginStatus> PasswordSignInAsync method instead.");
        }

        public virtual async Task<LoginStatus> PasswordSignInAsync(string userName, string password)
        {
            if (UserManager == null || AuthService == null)
            {
                return LoginStatus.Failure;
            }

            var userVerified = await AuthService.VerifyDotUser(new Credentials { Email = userName, Password = password });

            if (userVerified == null)
            {
                return LoginStatus.Failure;
            }

            if (userVerified.Status == LoginStatus.Success)
            {
                if (!userVerified.UserId.HasValue)
                {
                    return LoginStatus.Failure;
                }

                var user = DotUserSecurityInfo.CreateFromDotUserClaims(userVerified.DotUserClaims);
                // Values for isPersistent, rememberBrowser should be false always
                await SignInAsync(user, false, false);
            }

            return userVerified.Status;
        }
        
        public virtual void LogOff(int userId)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }
    }
}