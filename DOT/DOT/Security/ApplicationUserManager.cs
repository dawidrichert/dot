﻿using DOT.Services.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace DOT.Security
{
    public class ApplicationUserManager : UserManager<DotUserSecurityInfo, string>
    {
        public ApplicationUserManager(IUserStore<DotUserSecurityInfo, string> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new DotUserStore(new DotUserAuthService()));

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<DotUserSecurityInfo, string>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            return manager;
        }
    }
}