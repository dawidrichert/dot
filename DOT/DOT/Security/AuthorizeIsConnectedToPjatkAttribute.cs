﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DOT.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeIsConnectedToPjatkAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException(nameof(httpContext));
            }

            var owinContext = httpContext.GetOwinContext();
            if (owinContext == null)
            {
                throw new InvalidOperationException("httpContext.owinContext is null");
            }

            var authentication = httpContext.GetOwinContext().Authentication;
            if (authentication == null)
            {
                throw new InvalidOperationException("httpContext.owinContext.authentication is null");
            }

            var principal = httpContext.GetOwinContext().Authentication.User;
            if (principal == null)
            {
                throw new InvalidOperationException("httpContext.owinContext.authentication.user is null");
            }

            return principal.HasClaim(ClaimsPrincipalExtensions.DotIsConnectedToPjatkClaimName, true.ToString());
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext?.HttpContext?.Request != null && filterContext.HttpContext.Request.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Home", action = "Index" }));
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}