﻿using System.Linq;
using System.Security.Claims;

namespace DOT.Security
{
    public static class ClaimsPrincipalExtensions
    {
        public const string DotPermissionClaimName = "DotPermission";
        public const string DotIsConnectedToPjatkClaimName = "DotIsConnectedToPjatk";

        /// <summary>
        /// Extension method to check the bas permission
        /// </summary>
        /// <param name="claimsPrincipal">extended claims principal instance</param>
        /// <param name="permissions">permission or comma seprated permissions</param>
        /// <returns>if has all permissions then true, false otherwise</returns>
        public static bool HasPermission(this ClaimsPrincipal claimsPrincipal, string permissions)
        {
            if (string.IsNullOrWhiteSpace(permissions))
            {
                // If checking for empty permission then return that it has permission.
                return true;
            }

            return permissions.Split(',').All(p => claimsPrincipal.HasClaim(DotPermissionClaimName, p));
        }
        
        public static bool IsConnectedToPjatk(this ClaimsPrincipal claimsPrincipal)
        {
            var claim = claimsPrincipal.FindFirst(c => c.Type == DotIsConnectedToPjatkClaimName);
            return claim != null && bool.Parse(claim.Value);
        }
    }
}