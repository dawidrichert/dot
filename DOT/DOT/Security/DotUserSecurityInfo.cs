﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DOT.Domain.Models.Login;
using Microsoft.AspNet.Identity;

namespace DOT.Security
{
    public class DotUserSecurityInfo : IUser<string>
    {
        // Needs to be string as otherwise SignInManager has issues with null values
        public string Id { get; set; }

        /// <summary>
        /// This stores email as the username
        /// </summary>
        public string UserName { get; set; }
        public bool IsConnectedToPjatk { get; set; }

        public IList<string> Permissions { get; set; }
        
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            return await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
        }

        public int GetId()
        {
            return Convert.ToInt32(Id);
        }

        public static DotUserSecurityInfo CreateFromDotUserClaims(DotUserClaims dotUserClaims)
        {
            if (dotUserClaims != null)
            {
                return new DotUserSecurityInfo
                {
                    Id = Convert.ToString(dotUserClaims.Id),
                    UserName = dotUserClaims.Email,
                    IsConnectedToPjatk = dotUserClaims.IsConnectedToPjatk,
                    Permissions = new List<string>(dotUserClaims.Permissions)
                };
            }

            return null;
        }
    }
}