﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using DOT.Services.Services;
using Microsoft.AspNet.Identity;

namespace DOT.Security
{
    public class DotUserStore : IUserClaimStore<DotUserSecurityInfo, string>, IUserSecurityStampStore<DotUserSecurityInfo, string>
    {
        private readonly IDotUserAuthService _authService;

        public DotUserStore(IDotUserAuthService authService)
        {
            _authService = authService;
        }

        public void Dispose()
        {
            // nothing yet to do here.
        }
        
        public Task CreateAsync(DotUserSecurityInfo user)
        {
            throw new NotImplementedException("Not used");
        }

        public Task DeleteAsync(DotUserSecurityInfo user)
        {
            throw new NotImplementedException("Not used");
        }

        public Task UpdateAsync(DotUserSecurityInfo user)
        {
            throw new NotImplementedException("Not used");
        }

        private DotUserSecurityInfo _dotUser;

        public async Task<DotUserSecurityInfo> FindByIdAsync(string userId)
        {
            // This is run once per request now to load the user from services
            // Per each request there is a different instance of the DotUserStore so it's working fine.
            if (_dotUser == null || _dotUser.Id != userId)
            {
                var user = await _authService.GetDotUserById(Convert.ToInt32(userId));
                _dotUser = DotUserSecurityInfo.CreateFromDotUserClaims(user);
            }

            return _dotUser;
        }

        public async Task<DotUserSecurityInfo> FindByNameAsync(string userName)
        {
            var user = await _authService.GetDotUserByEmail(userName);
            return DotUserSecurityInfo.CreateFromDotUserClaims(user);
        }
        
        public async Task<IList<Claim>> GetClaimsAsync(DotUserSecurityInfo user)
        {
            IList<Claim> claims = new List<Claim>();

            claims.Add(new Claim(ClaimsPrincipalExtensions.DotIsConnectedToPjatkClaimName, user.IsConnectedToPjatk.ToString()));

            foreach (var perm in user.Permissions)
            {
                claims.Add(new Claim(ClaimsPrincipalExtensions.DotPermissionClaimName, perm));
            }

            return await Task.FromResult(claims);
        }

        public Task AddClaimAsync(DotUserSecurityInfo user, Claim claim)
        {
            throw new NotImplementedException("Not used");
        }

        public Task RemoveClaimAsync(DotUserSecurityInfo user, Claim claim)
        {
            throw new NotImplementedException("Not used");
        }

        public Task SetSecurityStampAsync(DotUserSecurityInfo user, string stamp)
        {
            throw new NotImplementedException("Not used");
        }

        public async Task<string> GetSecurityStampAsync(DotUserSecurityInfo user)
        {
            return await Task.FromResult(CalculateSecurityStampFromTheDotUser(user));
        }

        // Creates the stamp from the user object, if the user object changes somehow, i.e. permissions
        // then the stamp will be different and will force the user to log in again
        private string CalculateSecurityStampFromTheDotUser(DotUserSecurityInfo user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var stamp = new StringBuilder();

            stamp.Append(user.Id);
            stamp.Append(user.UserName);

            if (user.Permissions != null)
            {
                foreach (var perm in user.Permissions)
                {
                    stamp.Append(perm);
                }
            }

            // Just hash it to make it unreadable and smaller, does not need to be with a complex alghorithm 
            // as it's just to sign it and anyway the permissions are loaded each time. It is only
            // to assure the user cookie is invalid and requires the user to login again if something changes.
            // so MD5 will be fine here, maybe not that secure but fast and that's what's required here.
            // This method is called for each request.

            using (var md5Hash = MD5.Create())
            {
                var stampHash = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(stamp.ToString()));
                return Convert.ToBase64String(stampHash);
            }
        }

    }
}

