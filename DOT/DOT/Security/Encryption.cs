﻿using System;
using System.Text;
using System.Web.Security;

namespace DOT.Security
{
    public static class Encryption
    {
        public static string Protect(string text, string purpose)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                throw new ArgumentOutOfRangeException(nameof(text), @"Needs to have a non empty value");
            }

            if (string.IsNullOrWhiteSpace(purpose))
            {
                throw new ArgumentOutOfRangeException(nameof(purpose), @"Needs to have a non empty value");
            }

            var stream = Encoding.UTF8.GetBytes(text);
            var encodedValue = MachineKey.Protect(stream, purpose);
            return Convert.ToBase64String(encodedValue);
        }

        public static string Unprotect(string text, string purpose)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                throw new ArgumentOutOfRangeException(nameof(text), @"Needs to have a non empty value");
            }

            if (string.IsNullOrWhiteSpace(purpose))
            {
                throw new ArgumentOutOfRangeException(nameof(purpose), @"Needs to have a non empty value");
            }

            var stream = Convert.FromBase64String(text);
            var decodedValue = MachineKey.Unprotect(stream, purpose);

            if (decodedValue == null)
            {
                throw new NullReferenceException(nameof(decodedValue));
            }
            return Encoding.UTF8.GetString(decodedValue);
        }
    }
}
