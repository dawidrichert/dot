﻿using System;
using System.Security.Cryptography;

namespace DOT.Security
{
    public class PasswordHasher
    {
        public static readonly int DefaultPasswordHashIterations = 10000;
        private readonly int _iterationsCount;

        public PasswordHasher(int iterationsCount)
        {
            _iterationsCount = iterationsCount;
        }

        public string GenerateSalt()
        {
            var salt = Guid.NewGuid().ToByteArray();
            return ConvertSaltToString(salt);
        }

        public string HashPassword(string password, string saltBase64)
        {
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, ConvertSaltToByteArray(saltBase64))
            {
                IterationCount = _iterationsCount
            };

            var hash = rfc2898DeriveBytes.GetBytes(20);

            return Convert.ToBase64String(hash);
        }

        private static string ConvertSaltToString(byte[] salt)
        {
            return Convert.ToBase64String(salt);
        }

        private static byte[] ConvertSaltToByteArray(string saltBase64)
        {
            return Convert.FromBase64String(saltBase64);
        }
    }
}