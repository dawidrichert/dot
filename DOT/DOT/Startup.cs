﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DOT.Startup))]
namespace DOT
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
