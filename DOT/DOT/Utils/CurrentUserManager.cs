﻿using System.Web;
using Microsoft.AspNet.Identity;

namespace DOT.Utils
{
    public static class CurrentUserManager
    {
        public static int CurrentUserId => HttpContext.Current.GetOwinContext().Authentication.User.Identity.GetUserId<int>();
    }
}