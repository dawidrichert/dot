﻿using System.Globalization;
using System.Web;
using System.Web.Mvc;

namespace DOT.Utils
{
    public static class Localizations
    {
        public static readonly string LanguageCookieName = "Language";
        public static readonly string EnglishCultureCode = "en-GB";
        public static readonly string PolishCultureCode = "pl-PL";

        public static bool IsUserLanguageEnglish(HttpRequestBase request)
        {
            return request.Cookies[LanguageCookieName] == null || request.Cookies[LanguageCookieName].Value == EnglishCultureCode;
        }

        public static bool IsUserLanguagePolish(HttpRequestBase request)
        {
            return request.Cookies[LanguageCookieName] != null && request.Cookies[LanguageCookieName].Value == PolishCultureCode;
        }

        public static CultureInfo GetUserCultureInfo(ControllerContext context)
        {
            var request = context.HttpContext.Request;

            var code = request.Cookies[LanguageCookieName] == null 
                ? EnglishCultureCode 
                : request.Cookies[LanguageCookieName].Value;

            return new CultureInfo(code);
        }
    }
}