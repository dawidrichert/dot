﻿using System.ComponentModel.DataAnnotations;

namespace DOT.ViewModels
{
    public class ConnectAccountViewModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Global), Name = "Password")]
        public string Password { get; set; }
    }
}
