﻿using System.Collections.Generic;
using DOT.Domain.Models;

namespace DOT.ViewModels
{
    public class GradesViewModel
    {
        public string Name { get; set; }

        public ICollection<Grade> Grades { get; set; }
    }
}