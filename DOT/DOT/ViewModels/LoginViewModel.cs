﻿using System.ComponentModel.DataAnnotations;

namespace DOT.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Global), Name = "Password")]
        public string Password { get; set; }
    }
}