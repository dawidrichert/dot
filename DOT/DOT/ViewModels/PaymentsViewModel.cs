﻿using System.Collections.Generic;
using DOT.Domain.Models;

namespace DOT.ViewModels
{
    public class PaymentsViewModel
    {
        public ICollection<Payment> Payments { get; set; }
        public ICollection<Fee> Fees { get; set; }
        public StudentInfo StudentInfo { get; set; }
    }
}