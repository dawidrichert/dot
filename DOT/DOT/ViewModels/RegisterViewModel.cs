﻿using System.ComponentModel.DataAnnotations;

namespace DOT.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.Global), ErrorMessageResourceName = "ErrorMessageStringLengthAtLeast", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Global), Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Global), Name = "ConfirmPassword")]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Global), ErrorMessageResourceName = "PasswordAndConfirmationNotMatch")]
        public string ConfirmPassword { get; set; }
    }
}