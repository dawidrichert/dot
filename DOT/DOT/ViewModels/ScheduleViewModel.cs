﻿using System;
using System.Collections.Generic;
using DOT.Domain.Models;

namespace DOT.ViewModels
{
    public class ScheduleViewModel
    {
        public DateTime Date { get; set; }
        public ICollection<Schedule> Schedules { get; set; }
    }
}